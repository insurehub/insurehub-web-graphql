const{ApolloServer, gql} = require("apollo-server")
const _ = require("lodash")

const typeDefs = gql`
    type Query{
        getPolicy(id:String): [ApplicationDetails]
    }

    type ApplicationDetails {
        policyDetails: [PolicyDetails]
        customer: [Customer]
      }
    
      type PolicyDetails {
        insuranceType: String
        policyStatus: String
        dateCreated: String
        nextPremiumDue: String
        periodOfInsuranceFrom: String
        periodOfInsuranceTo: String
        productCode: String
        productDescription: String
        policyData: [PolicyData]
        premiumDetails: [PremiumDetails]
        policyConditions: [PolicyConditions]
      }

      type PolicyConditions {
        hasAgent: String
      }
    
      type PremiumDetails {
        nextPremiumDue: String
        isReinsured: String
        premiumAmount: String
        sumAssured: String
        reinsuranceAmount: String
        premiumFrequency: String
      }

      type PolicyData {
        lifeInsurance: LifeInsurance
        medicalInsurance: MedicalInsurance
        funeralInsurance: FuneralInsurance
        vehicleInsurance: VehicleInsurance
        farmingInsurance: FarmingInsurance
        householdInsurance: HouseholdInsurance
      }
    
      type HouseholdInsurance {
        coverConditions: CoverConditions
        propertyDetails: [PropertyDetails]
      }
    
      type PropertyDetails {
        propertyType: String
        propertyUse: String
        isLetToAFamily: String
        numberOfTenants: String
        isBuiltWithBricks: String
        materialsUsedToBuild: String
        isRoofedWithSlateOrTiles: String
        materialsUsedToRoof: String
        propertyHasPreservationOrder: String
        presevervationOrderDescription: String
        propertyHasUniqueFeatures: String
        uniqueFeatures: String
        yearHouseWasBuilt: String
        areaOfProperty: String
        areaOfPropertyDetails: String
        daysLeftUnoccupied: String
        propertyConditions: PropertyConditions
        address: [Address]
      }
    
      type PropertyConditions {
        isLeftUnoccupiedAtDay: String
        hasDeadlockDevices: String
        isFittedWithSecureLocks: String
        hasBurglarAlarm: String
        isMonitoredAllDay: String
        hasSmokeDetectors: String
        fullCostOfHome: String
        fullReplacementCostOfContents: String
        fireOnlyCover: String
        fullAccidentalDamageCover: String
      }
    
      type FarmingInsurance {
        farmProperty: [FarmProperty]
        farmConditions: FarmConditions
        farmingDetails: FarmingDetails
        farmProducts: FarmProducts
      }
    
      type FarmProducts {
        crops: [Crops]
        livestock: [Livestock]
      }
    
      type Livestock {
        yearOfBirth: String
        headNumber: String
        nameOfLiveStock: String
        valueOfStock: String
      }
    
      type Crops {
        valueOfCrops: String
        dateSown: String
        areaSown: String
        nameOfCrop: String
      }
    
      type FarmingDetails {
        farmingActivity: String
        farmerCategory: String
        farmerClassification: String
      }
    
      type FarmConditions {
        fireProtection: String
        securitySystem: String
        ownMoreThanOne: String
        fenced: String
      }
    
      type FarmProperty {
        propertyMake: String
        serialNumber: String
        yearPurchase: String
        propertyWorthy: String
        whereYouBought: String
      }
    
      type VehicleInsurance {
        vehicleDetails: VehicleDetails
      }
    
      type VehicleDetails {
        vehicleRegistrationNumber: String
        clientLicenseNumber: String
        dateOfLicenseIssue: String
        vehicleMake: String
        vehicleModel: String
        vehicleYear: String
        vehicleMileage: String
        carInsuranceType: String
        insuredAtMarketValue: Boolean
        excessAmount: String
        sumAssured: String
        vehicleConditions: VehicleConditions
      }
    
      type VehicleConditions {
        thirdPartyLiability: Boolean
        coversAllRisks: Boolean
        includesReplacementCar: Boolean
        hasTotalLoss: Boolean
        hasExcess: Boolean
      }
    
      type FuneralInsurance {
        benefits: Benefits
      }
    
      type Benefits {
        coffinAmount: String
        coffinType: String
        grocery: String
        includesTransport: String
        repatriation: String
        condolenceFee: String
        productType: String
        productCode: String
        extendedFamilyBenefits: String
      }
    
      type MedicalCondition {
        smoking: Boolean
        hasBloodPressure: Boolean
        physicalimpairment: String
        mentalimpairment: Boolean
        hadStroke: Boolean
        chronicConditon: [ChronicConditon]
      }
      
      type MedicalInsurance {
        medicalCondition: MedicalCondition
      }
    
      type ChronicConditon {
        chronicConditionCode: String
        chronicMedication: String
        yearOfDrugCommencement: String
        strengthOfDrugBeingTaken: String
      }
    
      type LifeInsurance {
        coverConditions: CoverConditions
      }
    
      type CoverConditions {
        doInsuredSmoke: String
        hasinsuredCommittedoffense: String
        isInsuredActiveatWork: String
        insuredMedicalCondition: String
      }    
    
      type Customer {
        person: [Person]
        address: [Address]
        relatedParty: [RelatedParty]
        entity: [Entity]
      }
    
      type Person {
        salutation: String
        nationalId: String
        clientFirstName: String
        clientLastName: String
        dateOfBirth: String
        clientGender: String
        maritalStatus: String
        identificationType: String
        emailAddress: String
        phoneNumber: String
        employmentDetails: [EmploymentDetails]
      }
    
      type EmploymentDetails {
        dateOfEmployment: String
        jobTitle: String
        grossSalary: String
        netSalary: String
        salaryFrequency: String
      }
    
      type RelatedParty {
        nationalId: String
        relationshipToMember: String
        firstName: String
        lastName: String
        dateOfBirth: String
        gender: String
        salutation: String
        identificationType: String
        entityRelatedPartyDetails: [EntityRelatedPartyDetails]
        contactDetails: [ContactDetails]
      }
    
      type EntityRelatedPartyDetails {
        jobTitle: String
      }
    
      type ContactDetails {
        emailAddress: String
        phoneNumber: String
        businessEmail: String
        role: String
        roleDescription: String
        dateOfEmployment: String
      }
    
      type Address {
        addressType: String
        addressZip: String
        streetAddress: String
        city: String
        region: String
        country: String
        suburb: String
      }
    
      type Entity {
        class: String
        name: String
        registrationNumber: String
        description: String
      }
`

//Creating instance data
const applicationDetails =[{"policyId": "A123"}]
const policyDetails =[
    {   
        "insuranceType": "Household",
        "policyStatus": "Active",
        "dateCreated": "2021-10-15",
        "nextPremiumDue": "2021-10-15",
        "periodOfInsuranceFrom": "2021-10-15",
        "periodOfInsuranceTo": "2021-10-15",
        "productCode": "PC12334",
        "productDescription": "String",
        "policyId": "A123"
    }
  ] // Add back policy Data etc
  
  const premiumDetails =[
      {
        "nextPremiumDue": "2021-10-15",
        "isReinsured": "String",
        "premiumAmount": "String",
        "sumAssured": "String",
        "reinsuranceAmount": "String",
        "premiumFrequency": "String",
        "policyId":"A123"
      }
  ]

  const policyConditions =[
    {
        "hasAgent": "true",
        "policyId": "A123"
    }
]
  // add customer somewhere
  const customers = [{"policyId": "A123"}]

  const people =[
    {
        "salutation": "Mr",
        "nationalId": "08-207643R21",
        "clientFirstName": "Brendon",
        "clientLastName": "Mpofu",
        "dateOfBirth": "2000-04-08",
        "clientGender": "Male",
        "maritalStatus": "Single",
        "identificationType": "String",
        "emailAddress": "email@gmail.com",
        "phoneNumber": "0776528761",
        "policyId": "A123"
    }
  ]
  
  const employmentDetails =[
    {
        "dateOfEmployment": "2020-10-12",
        "jobTitle": "manager",
        "grossSalary": "$10 000",
        "netSalary": "$8 000",
        "salaryFrequency": "monthly",
        "policyId": "A123"
    }
  ]
  
  const relatedParties =[
    {
        "nationalId": "08-3029181V21",
        "relationshipToMember": "brother",
        "firstName": "Tinashe",
        "lastName": "Mpofu",
        "dateOfBirth": "20-12-13",
        "gender": "Male",
        "salutation": "Mr",
        "identificationType": "String",
        "policyId": "A123"
    }
  ]
  
  const adresses =[
    {
        "addressType": "20436 Pumula South",
        "addressZip": "00263",
        "streetAddress": "String",
        "city": "Bulawayo",
        "region": "String",
        "country": "Zimbabwe",
        "suburb": "Pumula",
        "policyId": "A123"
    }
  ]
  
  const entityRelatedPartyDetails =[
      {
          "jobTitle": "Secretary",
          "policyId": "A123"
    }
  ]

  const contactDetails=[
      {
        "emailAddress": "email@gmail.com",
        "phoneNumber": "+26377653435",
        "businessEmail": "email@gmail.com",
        "role": "String",
        "roleDescription": "String",
        "dateOfEmployment": "2012-12-23",
        "policyId": "A123" 
      }
  ]
  const entities =[
    {
        "class": "String",
        "name": "String",
        "registrationNumber": "N01975X",
        "description": "String",
        "policyId": "A123"
    }
  ]

//Creating resolvers:
const resolvers ={
    Query:{
        getPolicy: (parent, args)=>{
            return _.filter(applicationDetails,{policyId:args.id})
            // return applicationDetails
        }
    },
    ApplicationDetails:{
        policyDetails:(parent)=>{
            return _.filter(policyDetails,{policyId:parent.policyId})
        },
        customer:(parent)=>{
            return _.filter(customers,{policyId:parent.policyId})
        }
    },
    PolicyDetails:{
        premiumDetails:(parent)=>{
            return _.filter(premiumDetails,{policyId:parent.policyId})
        },
        policyConditions:(parent)=>{
            return _.filter(policyConditions,{policyId:parent.policyId})
        }
    },
    Customer:{
        person:(parent)=>{
            return _.filter(people,{policyId:parent.policyId})
        },
        address:(parent)=>{
            return _.filter(adresses, {policyId:parent.policyId})
        },
        relatedParty:(parent)=>{
            return _.filter(relatedParties,{policyId:parent.policyId})
        },
        entity:(parent)=>{
            return _.filter(entities,{policyId:parent.policyId})
        }
    },
    Person: {
        employmentDetails: (parent)=>{
            return _.filter(employmentDetails,{policyId:parent.policyId})
        }
    },
    RelatedParty: {
        entityRelatedPartyDetails:(parent)=>{
            return _.filter(entityRelatedPartyDetails,{policyId:parent.policyId})
        },
        contactDetails:(parent)=>{
            return _.filter(contactDetails,{policyId:parent.policyId} )
        }
    }

}

// Creating a server instance 
const gqlServer = new ApolloServer({typeDefs, resolvers})
gqlServer.listen({port: process.env.PORT||4003})
.then(({url}) => {console.log(`graphQL server started on ${url}
                    Query at https://studio.apollographql.com/dev
`)})